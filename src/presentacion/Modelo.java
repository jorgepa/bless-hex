package presentacion;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import static logica.infoFile.convertHexToString;
import static logica.infoFile.getByteArrayFileContent;
import logica.Info;
import logica.infoFile;
import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;

public class Modelo implements Runnable {

    private VistaPrincipal ventanaPrincipal;
    private Thread hiloFiles;
    private Info info;
    private infoFile nFiles;
    private JFileChooser ofile;

    public void iniciar() {
        //[691, 520]
        getVentanaPrincipal().setSize(1080, 640);
        getVentanaPrincipal().setVisible(true);
        getInfo();
        getFile();
    }

    public Info getInfo() {
        if (info == null) {
            info = new Info();
        }
        return info;
    }

    public infoFile getFile() {
        if (nFiles == null) {
            nFiles = new infoFile(this);
        }
        return nFiles;
    }

    public VistaPrincipal getVentanaPrincipal() {
        if (ventanaPrincipal == null) {
            ventanaPrincipal = new VistaPrincipal(this);
        }
        return ventanaPrincipal;
    }

    public void iniFiles() {
        hiloFiles = new Thread(this);
        hiloFiles.start();
    }

    public void archivo() {
        int resultfile = ofile.showOpenDialog(ofile);
        if (resultfile == JFileChooser.APPROVE_OPTION) {
            try {
                byte[] file = getByteArrayFileContent("", ofile.getSelectedFile());
                //System.out.println("file: " + Arrays.toString(file));
                System.out.println("file: " + file.length);
                String[] index = new String[]{"", "", "", "", "", "", "", "", "", ""};
                //JScrollPane scrollpane = new JScrollPane(table);
                System.out.println("->" + getVentanaPrincipal().getTableP().getName());
                getVentanaPrincipal().getTableP().setEnabled(true);

                Object[][] rawData = new Object[index.length][file.length];
                int col = 0, fil = 0;
                System.out.println(Hex.encodeHex(file));
                for (int i = 0; i < file.length; i++) {
                    byte b = file[i];

                    String convertHexToString = convertHexToString(Integer.toHexString(b) + "");
                    System.out.println("b: " + b + " HEX: " + Integer.toHexString(b) + " ascii: " + convertHexToString);
                    getInfo().setHex(b);
                    getInfo().setAscii(convertHexToString);
                    System.out.println("col:" + col + " fil" + fil);
                    if (col < 10) {
                        getVentanaPrincipal().getTableP().setValueAt((Integer.toHexString(b).length() == 1 ? "0" + Integer.toHexString(b) : Integer.toHexString(b)).toUpperCase(), fil, col);
                        rawData[fil][col] = b;
                    } else {
                        col = -1;
                        fil++;
                    }
                    col++;
                }

            } catch (Exception e) {
            }
        } else {
            System.out.println("No se cargo el archivo");
        }
    }

    public void iniFile() {
        try {

            ofile = new JFileChooser();
            ofile.setCurrentDirectory(new File("./temp"));
            int resultfile = ofile.showOpenDialog(ofile);
            if (resultfile == JFileChooser.APPROVE_OPTION) {
                File origignal = ofile.getSelectedFile();
                System.out.println("origignal:" + origignal);
                System.out.println("nFiles:" + nFiles);
                nFiles.splitFile(origignal.getAbsoluteFile());
            }
        } catch (Exception e) {
            System.out.println("run() e:" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        iniFile();
    }
}
